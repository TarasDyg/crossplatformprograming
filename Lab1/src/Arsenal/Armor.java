package Arsenal;
public class Armor extends  Equipment {
    private int defenceLevel;
    private  ArmorType type;
    public Armor( int serialNumber, String name, double weight, int defenceLevel, ArmorType type ){
        super(serialNumber,name,weight);
        this.defenceLevel=defenceLevel;
        this.type=type;
    }
    public int getLevel(){
        return  defenceLevel;
    }
    public ArmorType getArmorType() {
        return  this.type;
    }
    public int getEquipmentType(){
        return  type.ordinal();
    }
    @Override
    public String toString() {
        return  super.toString()+
                "defenceLevel=" + defenceLevel +
                ", type=" + type+"\n";
    }
}
