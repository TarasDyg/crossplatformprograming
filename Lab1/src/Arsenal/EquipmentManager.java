package Arsenal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.function.Predicate;

public class EquipmentManager {
    public static Object NameComparator;
    private ArrayList<Equipment> equipmentList;
    public static Comparator<Equipment> serialNumberComparator=(Equipment o1, Equipment o2)->o1.getSerialNumber()-o2.getSerialNumber();
    public ArrayList<Equipment> FindEquipmentForScoutMission(Predicate<Equipment> predicate){
        ArrayList<Equipment> tempList=new ArrayList<Equipment>();
        for (Equipment equipment : equipmentList) {
            if (predicate.test(equipment))
                tempList.add(equipment);
        }
        return  tempList;
    }
    public boolean AddEquipment(Equipment equipment){
        return equipmentList.add(equipment);
    }
    public EquipmentManager(){
        equipmentList=new ArrayList<Equipment>();
        equipmentList.clear();
    }
    public ArrayList<Equipment> sortEquipment(ArrayList<Equipment> equipmentList,Comparator<Equipment> comparator , boolean direction){
            if(direction){
                equipmentList.sort(comparator);
            }else {
                equipmentList.sort(comparator.reversed());
            }
        return equipmentList;
    }
    @Override
    public String toString() {
        String temp=new String();
        for (Equipment equipment: equipmentList) {
            temp+=equipment.toString();
        }
        return temp;
    }

    public static  class NameComparator implements Comparator<Equipment> {
        @Override
        public Comparator<Equipment> reversed() {
            return Collections.reverseOrder(this);
        }
        public int compare(Equipment o1, Equipment o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
