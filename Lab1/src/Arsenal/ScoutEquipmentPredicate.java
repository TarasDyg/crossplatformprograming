package Arsenal;
import java.util.HashMap;
import java.util.function.Predicate;
public class ScoutEquipmentPredicate implements Predicate<Equipment> {
    private HashMap<WeaponType,Integer> weaponTypeHashMap;
    private HashMap<ArmorType,Integer> armorTypeHashMap;
    public ScoutEquipmentPredicate(int persons){
        this.weaponTypeHashMap=new HashMap<WeaponType,Integer>();
        this.armorTypeHashMap=new HashMap<ArmorType,Integer>();
        weaponTypeHashMap.put(WeaponType.RIFLE,3);
        weaponTypeHashMap.put(WeaponType.ASAULTRIFLE,persons);
        weaponTypeHashMap.put(WeaponType.PISTOL,persons);
        weaponTypeHashMap.put(WeaponType.GRANADELAUNCHER,1);
        if(persons>10) {
            weaponTypeHashMap.put(WeaponType.MACHINEGUN, (int) (persons / 10));
        }
        armorTypeHashMap.put(ArmorType.BULLETPROOFVEST,persons);
        armorTypeHashMap.put(ArmorType.HELMET,persons);
    }
    @Override
    public boolean test(Equipment equipment) {
        if(equipment!=null) {
            if (equipment.getClass() == Weapon.class) {
                var test=((Weapon) equipment).getWeaponType();
                if (weaponTypeHashMap.containsKey(test)) {
                    if (weaponTypeHashMap.get(test) > 0) {
                        weaponTypeHashMap.put(((Weapon) equipment).getWeaponType(), weaponTypeHashMap.get(((Weapon) equipment).getWeaponType()) - 1);
                        return true;
                    }
                }
            } else if (equipment.getClass() == Armor.class) {
                var test=((Armor) equipment).getArmorType();
                if (armorTypeHashMap.containsKey(test)) {
                    if (armorTypeHashMap.get(test) > 0) {
                        armorTypeHashMap.put(test, armorTypeHashMap.get(test) - 1);
                        return true;
                    }
                }
            }
        }
        return  false;
    }
}
