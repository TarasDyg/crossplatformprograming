package Arsenal;
public class Weapon extends Equipment {
    private WeaponType type;
    private String caliber;
    public Weapon(int serialNumber, String name,double weight, String caliber, WeaponType type) {
      super(serialNumber,name,weight);
      this.type=type;
      this.caliber=caliber;

    }
    public Weapon() {

    }
    public WeaponType getWeaponType(){
        return type;
    }
    public String getCaliber(){
        return caliber;
    }
    public int getEquipmentType(){
        return  type.ordinal();
    }

    @Override
    public String toString() {
        return super.toString() +
                "type=" + type +
                ", caliber='" + caliber + "\n";
    }
}