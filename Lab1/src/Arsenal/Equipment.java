package Arsenal;
public abstract   class Equipment {
    private int serialNumber;
    private String name;
    private double weight;
    public Equipment(){

    }
    public Equipment( int serialNumber, String name, double weight){
        this.weight=weight;
        this.serialNumber=serialNumber;
        this.name=name;
    }
    public double getWeight() {
        return weight;
    }
    public String getName() {
        return name;
    }
    public int getSerialNumber() {
        return serialNumber;
    }
    public abstract int getEquipmentType();

    @Override
    public String toString() {
        return  "serialNumber=" + serialNumber +
                ", name='" + name + '\'' +
                ", weight=" + weight;

    }
}
