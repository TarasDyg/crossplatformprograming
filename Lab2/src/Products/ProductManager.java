package Products;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProductManager {
    private ArrayList<Product> productList1;
    private ConsoleInterface console;
    private ReadFromFile readFromFile;
    public  ProductManager(){
        productList1=new ArrayList<Product>();
    }
    public void SetConsoleInterface(ConsoleInterface consoleInterface){
        this.console=consoleInterface;
    }
    public HashMap<String,ArrayList<String[]>> OutputProducts(){
        HashMap<String,ArrayList<String[]>> productsByEndDate=new HashMap<String,ArrayList<String[]>>();
        ArrayList<LocalDate> endDates=GetEndDates();
        for (LocalDate endDate: endDates) {
            ArrayList<String[]>products=new ArrayList<String[]>();
            for (Product product: GetProductsByEndDate(endDate)) {
                products.add(product.toTableRow());
            }
            productsByEndDate.put(endDate.toString(),products);
        }
        return productsByEndDate;
    }
    public ArrayList<LocalDate> GetEndDates(){
        ArrayList<LocalDate> endDates=new ArrayList<>();
        for (Product product: productList1) {
            if (!endDates.contains(product.getEndDate())){
                endDates.add(product.getEndDate());
            }
        }
        return endDates;
    }
    public int MainLoop(){
        Commands command=Commands.NONE;
        try {
        while(true){
          command=console.GetUserCommand();
            switch (command){
                case EXIT: return 0;
                case PRINTBASE:{
                    String fileName=console.GetFileName();
                    readFromFile=new ReadFromFile(fileName);
                    if(readFromFile.ValidateFile()){
                        SetProductList(readFromFile.ReadProducts());
                    }
                    break;
                }
                case DELETE: {
                    ClearAllProductsWithName(console.GetProductName());
                    break;
                }
                case GETUNIQUE:{
                    ReadFromFile file1=new ReadFromFile(console.GetFileName());
                    ReadFromFile file2=new ReadFromFile(console.GetFileName());
                            if(file1.ValidateFile()&&file2.ValidateFile()){
                                productList1=GetUniqProductsFromTwoCollection(GetProducts(file1.ReadProducts()),GetProducts(file2.ReadProducts()));
                                DeleteProductsCreatedNotInThisYear();
                            }
                    break;
                }
                case REVALUATE:{
                    RevaluationProducts(0.1f);
                    break;
                }

            }if (command!=Commands.NONE) {
                console.SetData();
                console.PrintProgramScreen();
            }
        }
        }catch (Exception exception){
         return 1;
        }
    }

    public  void RevaluationProducts(double percent){
        LocalDate currentDate= LocalDate.now();
        productList1.stream().forEach(product -> {
            if(product.getEndDate().getDayOfYear()==(currentDate.getDayOfYear()+3)){
                product.setCost(product.getCost()*percent);
            }
        });
    }
    public  ArrayList<Product> GetUniqProductsFromTwoCollection(ArrayList<Product> productList1,ArrayList<Product> productList2){
       ArrayList<Product> temp= new ArrayList<Product>();
        for (Product product: productList1) {
            if((!temp.contains(product))&&(!productList2.contains(product))){
                temp.add(product);
            }
        }
        for (Product product: productList2) {
            if((!temp.contains(product))&&(!productList1.contains(product))){
                temp.add(product);
            }
        }
        return temp;

    }
    public void DeleteProductsCreatedNotInThisYear(){
        LocalDate currentDate=LocalDate.now();
        productList1.removeIf(product->(product.getCreatingDate().getYear()!=currentDate.getYear()));
    }
    public  void ClearAllProductsWithName(String name){
        productList1.removeIf(product-> Objects.equals(name,product.getName()));
    }
    public void SetProductList(ArrayList<String> productsString){
        productList1=GetProducts(productsString);
    }
    public ArrayList<Product> GetProductsByEndDate(LocalDate endDate){
        List<Product> temp= productList1.stream()
                .filter(product -> (product.getEndDate().equals(endDate)))
                .collect(Collectors.toList());
        return new ArrayList<Product>(temp);
    }
    public void SetUniqProductsFromTwoCollection(ArrayList<String> productList1,ArrayList<String> productList2){
        this.productList1=GetUniqProductsFromTwoCollection(GetProducts(productList1),GetProducts(productList2));
    }
    public ArrayList<Product> GetProducts(ArrayList<String> productsString){
        ArrayList<Product> productList=new ArrayList<Product>();
            for (String productString : productsString) {
                Product temp= GetProductFromString(productString);
                if (temp!=null){
                    productList.add(temp);
                }
            }
            return productList;
    }
    private Product GetProductFromString(@NotNull String productString){
        String tempName;
        LocalDate tempCreatingDate;
        LocalDate tempEndDate;
        double tempCost;
        String[] tokens=productString.split(" ");
       try {
           DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
           tempName = tokens[0];
           tempCreatingDate = LocalDate.parse(tokens[1],formatter);
           tempEndDate = LocalDate.parse(tokens[2],formatter);
           tempCost = Double.parseDouble(tokens[3]);
           return new Product(tempName, tempCreatingDate, tempEndDate, tempCost);

       }catch (java.time.format.DateTimeParseException exception) {
           ConsoleInterface.PrintException(exception);
           return null;
       }
    }


}
