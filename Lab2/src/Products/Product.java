package Products;

import java.time.LocalDate;

public class Product {
    private String name;
    private LocalDate creatingDate;
    private LocalDate endDate;
    private double cost;
    public Product(String name, LocalDate creatingDate, LocalDate endDate,double cost){
        this.name=name;
        this.creatingDate=creatingDate;
        this.endDate=endDate;
        this.cost=cost;
    }
    public String getName() {
        return name;
    }
    public LocalDate getEndDate(){
        return  endDate;
    }

    public LocalDate getCreatingDate(){
        return creatingDate;
    }
    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
    public boolean equals(Product product) {
        return this.name.equals(product.getName());
    }
    public String[] toTableRow(){
      return new  String[]{name,creatingDate.toString(),endDate.toString(),String.valueOf(cost) };
    }
    @Override
    public String toString() {
        return name+ " "+creatingDate+" "+ endDate+" "+ cost;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
    @Override
    public boolean equals(Object obj) {
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
        Product other=(Product)obj;
        if(other.getName().equals(name)){
            return true;
        }
       return (other.getName()==this.name);
    }
}
