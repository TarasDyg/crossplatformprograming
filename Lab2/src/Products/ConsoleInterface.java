package Products;

import java.util.*;
import java.util.concurrent.ConcurrentMap;

public class ConsoleInterface {
    private ProductManager productManager;
    private String verticalSep;
    private String joinSep;
    private HashMap<String, ArrayList<String[]>> rows = new HashMap<String, ArrayList<String[]>>();
    private String VerticalSep="|";
    private Scanner scanner;
    public ConsoleInterface(ProductManager productManager) {
        this.productManager=productManager;
        verticalSep="|";
        joinSep="+";
        this.scanner=new Scanner(System.in);
    }
    public void SetData(){
       rows= productManager.OutputProducts();
    }
    private void PrintHorizontalLine(int[] columnWidths){
        for (int i = 0; i < columnWidths.length; i++) {
            String line = String.join("", Collections.nCopies(columnWidths[i] +
                    verticalSep.length() + 1, "-"));
            System.out.print(joinSep + line + (i == columnWidths.length - 1 ? joinSep : ""));
        }
        System.out.println();
    }
    private void PrintRow(Map.Entry<String,ArrayList<String[]>> row,int[] maxWidths){
        String str=row.getKey();
        System.out.printf("%s %-" + maxWidths[0] + "s %s", verticalSep, str, "");
        boolean b=false;
        for (String [] partRow: row.getValue()) {
            if(b){
                System.out.println();
                System.out.printf("%s %-" + maxWidths[0] + "s %s", verticalSep," " , "");
            }
            for (int i=0;i<partRow.length;++i){
                String varStrTemp="";
                str=partRow[i];
                if(partRow.length==(i+1)){
                    varStrTemp="|";
                }
                System.out.printf("%s %-" + maxWidths[i+1] + "s %s", verticalSep, str, varStrTemp);
            }
            b=true;
        }
        System.out.println();
    }


    public void PrintTable(){
        int[] maxWidths=null;
        for (Map.Entry<String,ArrayList<String[]>>cells: rows.entrySet()) {
            if(maxWidths==null){
               maxWidths=new int[cells.getValue().get(0).length+1];
            }
            maxWidths[0]=Math.max(maxWidths[0], cells.getKey().length());
            for (String[] partCells : cells.getValue()) {
                for (int i=0;i<cells.getValue().get(0).length;++i){
                    maxWidths[i+1]=Math.max(maxWidths[i+1],partCells[i].length());
                }
            }
        }
        if (maxWidths!=null) {
            for (Map.Entry<String, ArrayList<String[]>> cells : rows.entrySet()) {
                PrintHorizontalLine(maxWidths);
                PrintRow(cells, maxWidths);
            }
            PrintHorizontalLine(maxWidths);
        }
    }
    public void PrintProgramScreen(){
        PrintTable();
        PrintHelps();
    }
    public void PrintHelps(){
        System.out.println( "To print table of products from file press :P ");
        System.out.println("revaluate products press :R");
        System.out.println("To delete all products with entered name press :D");
        System.out.println("To print table of unique products from to 2 files press :U");
        System.out.println("To exit press :E");
    }

    public static void  PrintException(Exception exception){
        System.out.print(exception.toString()+"\n");
    }

    public Commands GetUserCommand(){
        String str=scanner.next().toLowerCase();
        char command=str.charAt(0);
        switch (command){
            case 'e': return Commands.EXIT;
            case 'u': return Commands.GETUNIQUE;
            case 'd': return Commands.DELETE;
            case 'r': return Commands.REVALUATE;
            case 'p': return Commands.PRINTBASE;
            default : return Commands.NONE;
        }
    }
    public String GetFileName(){
        System.out.println("Enter name of input information file");
        String str =scanner.next();
        return str;
    }
    public String GetProductName(){
        System.out.println("Enter name of products you want delete");
        String str =scanner.next();
        return str;
    }


}
