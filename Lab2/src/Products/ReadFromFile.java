package Products;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileSystemException;
import java.rmi.NoSuchObjectException;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFromFile {
    private File dateFile;
    public  ReadFromFile(String fileName){
        dateFile=new File(fileName);
    }
    public boolean ValidateFile(){
        try{
            if(dateFile==null){
                throw new NoSuchObjectException("dateFile is null");
            }
            if(!dateFile.exists()){
                throw new FileNotFoundException("File with name "+dateFile.getName()+"not exist");
            }
            if(dateFile.length()<=0){
                throw new FileSystemException("file is clear");
            }
        }catch ( Exception exception){
            ConsoleInterface.PrintException(exception);
            return false;
        }
        return true;
    }
    public ArrayList<String> ReadProducts(){
        ArrayList<String> temp = new ArrayList<String>();
        try{
        if (ValidateFile()){
            Scanner reader = new Scanner(dateFile);
            while(reader.hasNextLine()) {
                temp.add(reader.nextLine());
            }
            return temp;
        }else {
            return null;
        }
        }catch (FileNotFoundException exception){
            ConsoleInterface.PrintException(exception);
            return null;
        }
    }

    @Override
    public String toString() {
        if(ValidateFile()) {
            return dateFile.getName()+"\n"+dateFile.getAbsolutePath()+"\n";
        }
        else {
            return "error";
        }
    }
}
