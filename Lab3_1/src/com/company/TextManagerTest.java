package com.company;

import java.io.File;
import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class TextManagerTest {

    @org.junit.jupiter.api.Test
    void deleteEnglishArticles() throws FileNotFoundException {
       ReadFile file=new ReadFile("C:\\Users\\Taras\\source\\repos\\CPP\\Lab3_1\\src\\test2.txt");
        TextManager text=new TextManager(file.ReadDataFromFile());
        text.DeleteEnglishArticles();
        assertFalse(text.toString().matches("\\ba\\b|\\bis\\b|\\bon\\b|\\bor\\b|\\bare\\b|\\bof\\b|\\bin\\b|\\bout\\b"));
    }
}