package com.company;
import org.jetbrains.annotations.NotNull;

import java.util.stream.Stream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TextManager {
    private String text;
    private ArrayList <HashMap<Integer,Integer>>BracketIndexes;
    public ArrayList<Integer> openBracketsIndex;
    public ArrayList<Integer> closeBracketsIndex;
    public TextManager(String text){
        this.text=text;
        BracketIndexes=new ArrayList <HashMap<Integer,Integer>>();
    }
    public void  setText(String text){
        this.text=text;
    }
    public ArrayList<String> TextInBrackets(){
       text=ReplaysNubersInText(text);
        FindBracketsIndexes();
        ArrayList<String> textInBrackets=new ArrayList<String>();
        int openBracketCount=0;
        int closeBracketCount=0;
        while((openBracketsIndex.size()>openBracketCount)&&(openBracketsIndex.size()>closeBracketCount)){
            int tempOpenBracket =openBracketsIndex.get(openBracketCount);
            int tempCloseBracket=closeBracketsIndex.get(closeBracketCount);
            int tempOpen=openBracketCount;
            int closeCount=0;
            int openCount=0;
            int tempClose=closeBracketCount;
            do {
                if ((openBracketsIndex.size() <= (tempOpen+1)) || (closeBracketsIndex.size() <= (tempClose+1))) {
                    if(openBracketsIndex.size()<=(tempOpen+1)){
                        tempClose=tempClose+(openCount-closeCount);
                        closeCount=openCount;
                    }
                    break;
                }
                if(openBracketsIndex.get(tempOpen)>closeBracketsIndex.get(tempClose)){
                    ++tempClose;
                    closeCount=0;
                }else {

                    if (closeBracketsIndex.get(tempClose) > openBracketsIndex.get(Math.min( tempOpen+1,openBracketsIndex.size()-1))) {
                        ++tempOpen;
                        ++openCount;
                    } else if (openCount > closeCount) {
                        ++tempClose;
                        ++closeCount;
                    }
                }
            }while((openCount!=closeCount)||(closeBracketsIndex.get(tempClose) > openBracketsIndex.get(Math.min( tempOpen+1,openBracketsIndex.size()-1))));
            if(openCount==closeCount) {
                int count=0;
                int tempCount=0;
                int j=0;
                int openBracketIndex=tempOpen;
                int closeBracketIndex=tempClose;
                for (int i=closeBracketCount; i<tempClose;++i){
                    for ( j=openBracketCount;j<tempOpen;++j){
                        if(openBracketsIndex.get(j)<closeBracketsIndex.get(i)){
                            ++count;
                            openBracketIndex=j;
                        }else {
                            break;
                        }
                    }
                    if ((count-(i-closeBracketCount))>tempCount) {
                        tempCount = count - (i - closeBracketCount);
                        if (openBracketCount!=openBracketIndex){
                            closeBracketIndex=i;
                        }
                    }
                    count=0;
                }
                if(openBracketIndex!=openBracketCount){
                    String deletedString=text.substring(openBracketsIndex.get(openBracketIndex)+1,closeBracketsIndex.get(closeBracketIndex));
                 text=text.replace(deletedString,"");
                    FindBracketsIndexes();
                     tempOpenBracket =openBracketsIndex.get(openBracketCount);
                     tempCloseBracket=closeBracketsIndex.get(closeBracketCount);

                }
                textInBrackets.add(text.substring(tempOpenBracket+1, closeBracketsIndex.get(tempClose)));
                openBracketCount=tempOpen+1;
                closeBracketCount=tempClose+1;
            }else {
                ++openBracketCount;
            }
        }
        return textInBrackets;
    }
    private String ReplaysNubersInText(@NotNull String text){
       String out= new String(text);
        StringBuilder number = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if ((Character.isDigit(c)||((c=='.')&&(number.length()>0)))) {
                number.append(c);
            } else if (number.length() > 0){
               out=out.replace(number,"#");
               number= new StringBuilder();
            }
        }
        return out;
    }
    public void FindBracketsIndexes(){
        int j=0;
        if(text!=null) {
            if (text.length() > 0) {
                openBracketsIndex = new ArrayList<Integer>(IntStream
                        .range(0, text.length())
                        .filter(i -> (text.toCharArray()[i] == '('))
                        .boxed().collect(Collectors.toList()));
                closeBracketsIndex = new ArrayList<Integer>(IntStream
                        .range(0, text.length())
                        .filter(i -> (text.toCharArray()[i] == ')'))
                        .boxed().collect(Collectors.toList()));
            }
        }
    }
    public void DeleteEnglishArticles(){
        text=text.replaceAll("\\ba\\b|\\bis\\b|\\bon\\b|\\bor\\b|\\bare\\b|\\bof\\b|\\bin\\b|\\bor\\b|\\bout\\b","");
        text=text.replaceAll("\\b   \\b|\\b  \\b"," ");
    }
    @Override
    public String toString() {
        return text;
    }
}
